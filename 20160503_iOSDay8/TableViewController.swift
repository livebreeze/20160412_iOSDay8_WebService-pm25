//
//  TableViewController.swift
//  20160503_iOSDay8
//
//  Created by ChenSean on 5/3/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    // swift 變數宣告一定要給一個初始值，就給 nil 吧
    var list: NSArray? = nil
    var n = 0
    
    // 決定呼叫的時候，NSData 可能是 nil，所以給 ?(問號) ， ->(減右鍵頭)就是傳回的資料型態
    func parser(data: NSData?) -> AnyObject? {
        // swift 新的 if 判斷語法 guard
        guard data != nil else {
            return nil
        }
        
        // 再進一步偷吃步的寫法
        guard let data = data else {
            return nil
        }
        
        do {
            return try NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers)
        } catch {
            return nil
        }
    }
    
    func loadData(){
        let url = NSURL(string: "http://opendata.epa.gov.tw/ws/Data/AQX/?format=json")
        
        if let url = url {
            let data = NSData(contentsOfURL: url)
            if let jsonObj = parser(data) {
                list = jsonObj as? NSArray
            }
        }
    }
    
    func handleRefresh(){
        //重 load data
        loadData()
        n += 1
        
        title = "<台灣 PM2.5> 刷" + String(n) + "次"
        
        // 停止 refreshing 的動畫。
        refreshControl?.endRefreshing()
        //更新資料
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "<台灣 PM2.5> "
        loadData()
        
        //跟表格註冊重 refresh (預設 refreshControl 會是nil)
        refreshControl = UIRefreshControl()
        // 註冊 valueChanged events, 發生的時候執行 handleRefresh 動作
        refreshControl?.addTarget(self, action: #selector(handleRefresh), forControlEvents: .ValueChanged)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let list = list {
           return list.count
        }
        
        return 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        let data = list![indexPath.row]
        let county = data["County"] as! String
        let siteName = data["SiteName"] as! String
        let pm25 = data["PM2.5"] as! String
        
        let attriCounty = NSMutableAttributedString(string: county)
        let attriSiteName = NSMutableAttributedString(string: siteName)
            
        attriSiteName.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(12), range: NSMakeRange(0, attriSiteName.length))
        attriSiteName.addAttribute(NSForegroundColorAttributeName, value: UIColor.blueColor(), range: NSMakeRange(0, attriSiteName.length))
        
        let tmp = NSMutableAttributedString()
        tmp.appendAttributedString(attriCounty)
        tmp.appendAttributedString(attriSiteName)
        
        cell.textLabel?.attributedText = tmp;
        cell.detailTextLabel?.text = "PM 2.5 = " + pm25
        
        if (Int(pm25) < 0) {
            cell.backgroundColor = UIColor.grayColor()
        } else if (Int(pm25) < 36) {
            cell.backgroundColor = UIColor.greenColor()
        } else if (Int(pm25) < 54){
            cell.backgroundColor = UIColor.yellowColor()
        } else if (Int(pm25) < 71){
            cell.backgroundColor = UIColor.redColor()
        } else if (Int(pm25) >= 71){
            cell.backgroundColor = UIColor.purpleColor()
            
            let alert = UIAlertView()
            alert.title = "DANGER!"
            alert.message = county + siteName + " PM2.5 紫爆嘞！"
            alert.addButtonWithTitle("OK")
            alert.show()
        }
        
        return cell;
    }
}
